'use strict'

const Schema = use('Schema')

class UsersTableSchema extends Schema {

  up () {
    this.createIfNotExists('users', (table) => {
      table.increments('users_id').comment('使用者流水號')
      table.string('users_name', 32).comment('使用者名稱').notNull()
      table.string('users_email', 64).comment('使用者email').notNull()
      table.string('users_password').comment('使用者密碼').notNull()
      table.tinyint('users_gender', 1).comment('使用者性別').defaultTo(0)
      table.tinyint('users_is_del', 1).comment('是否刪除').defaultTo(0)
      table.tinyint('users_type', 1).comment('使用者類型').defaultTo(0)
      table.integer('edited_users').comment('編輯人員')
      table.timestamps()
    })
  }

  down () {
    this.table('users', (table) => {
      // opposite of up goes here
    })
  }

}

module.exports = UsersTableSchema
