'use strict'

const Lucid = use('Lucid')

class Message extends Lucid {

    static get table () {
        return 'messages'
    }

}

module.exports = Message
