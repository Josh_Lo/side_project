'use strict'

/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| AdonisJs Router helps you in defining urls and their actions. It supports
| all major HTTP conventions to keep your routes file descriptive and
| clean.
|
| @example
| Route.get('/user', 'UserController.index')
| Route.post('/user', 'UserController.store')
| Route.resource('user', 'UserController')
*/

const Route = use('Route')

Route.on('/').render('welcome')
Route.get('/user', 'UserController.index')
Route.get('/user/userList', 'UserController.lists')
Route.any('/user/userAdd', 'UserController.addData')
Route.get('/messageBoard', 'MessageController.index')
Route.get('/messageBoard/create', 'MessageController.create')
Route.get('/messageBoard/store', 'MessageController.store')
Route.get('/messageBoard/posts/:id', 'MessageController.show')