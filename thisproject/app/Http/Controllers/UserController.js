'use strict'

const User = use('App/Model/User')

class UserController {
  * index (request, response) {
		response.redirect('/user/userList')
	}

  * lists (request, response) {
	const users = yield User.all()
    yield response.sendView('user.list', {users: users.toJSON()})
  }

  * addData (request, response) {
  	if(request.post()){
  		const postData = request.only('users_name', 'users_email', 'users_password')
  		yield User.create(postData)
  	}
    yield response.sendView('user.addform')
  }
}

module.exports = UserController
