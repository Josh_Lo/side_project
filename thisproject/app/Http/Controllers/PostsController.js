'use strict'

class PostsController {

    * index(request, response) {
        yield response.sendView('welcome')
    }

}

module.exports = PostsController
