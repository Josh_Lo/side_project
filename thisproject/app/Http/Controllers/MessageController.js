'use strict'

const Post = use('App/Model/Message');
const Validator = use('Validator')

class MessageController {

    * index(request, response) {
        const posts = yield Post.query().orderBy('created_at', 'desc').fetch();
        yield response.sendView('message.board', {posts: posts.toJSON()} )
    }

    * create(request, response) {
        yield response.sendView('message.create')
    }

    * store(request, response) {
        const postData = request.only('title', 'name', 'content')

        const rules = {
          title: 'required',
          name: 'required',
          content: 'required'
        }

        const validation = yield Validator.validate(postData, rules)

        if (validation.fails()) {
            yield request
                .withOnly('title', 'name', 'content')
                .andWith({ errors: validation.messages() })
                .flash()

            response.redirect('back')
            return
        }

        yield Post.create(postData)
        response.redirect('/messageboard')
    }

    * show (request, response) {
        const post = yield Post.find(request.param('id'))
        yield response.sendView('message.show', { post: post.toJSON() })
    }

}

module.exports = MessageController
